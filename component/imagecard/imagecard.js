// component/imagecard/imagecard.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    item: {
      type: Object,
      value: {
        title: '图片标签',
        url: '图片链接',
        regedit: 0
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    clickHandle(e) {
      this.triggerEvent("clickHandle", this.data.item);
    },
    saveHandle(e) {
      this.triggerEvent('saveHandle', this.data.item);
    }
  }
})
