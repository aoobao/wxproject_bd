const util = require('../../utils/util.js')
//index.js
//获取应用实例
const app = getApp()

const navlist = getNavItem();

Page({
  data: {
    navItems: navlist,
    imgList: [],
    navBtnSelectIdx: 0,
    page: 0,
    mid: navlist[0].title,
    scrollTop: 1,
    scrollHeight: 0,
    loading: false,
    pageCount: 1
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;

    util.ready(() => {
      self.loadMoreEvent();
    });

    wx.getSystemInfo({
      success(res) {
        console.info(res.windowHeight);
        self.setData({
          scrollHeight: res.windowHeight - 45
        });
      }
    });
  },
  scroll: function (e) {
    //console.log(e.detail);
    // this.setData({
    //   scrollTop: e.detail.scrollTop
    // })
  },
  //标签页改变
  navItemTap(e) {
    const item = e.target.dataset.item;
    const index = e.target.dataset.index;
    console.log(this.data.scrollTop);
    if (index != this.data.navBtnSelectIdx) {

      this.setData({ navBtnSelectIdx: index, page: 0, mid: item.title, imgList: [], pageCount: 1 });
      this.loadMoreEvent();
    }
  },
  // 加载更多
  loadMoreEvent() {
    return new Promise((resolve, reject) => {
      var self = this;
      if (this.data.page < this.data.pageCount) {
        this.setData({
          loading: true
        });
        wx.showNavigationBarLoading() //在标题栏中显示加载
        var ids = self.data.imgList.map(t => t.cid);

        util.request({
          url: 'api/video/GetVideoListRand',
          data: {
            type: this.data.mid,
            size: util.PAGE_SIZE,
            ids: ids.toString()
            // page: this.data.page + 1,
            //pageSize: util.PAGE_SIZE
          },
          success(rst) {
            console.log(rst);
            if (rst.success) {
              const list = rst.Data.map(t => { return { url: t.ImageUrl, title: t.Code, cid: t.Id, regedit: t.Regedit, name: t.Title }; });
              var obj = {
                imgList: [...self.data.imgList, ...list],
                loading: false,
              };
              if (list.length < util.PAGE_SIZE) obj.page = 1;
              self.setData(obj)
            }
          },
          complete() {
            self.setData({ loading: false });
            wx.hideNavigationBarLoading() //完成停止加载
            resolve();
          }
        })

      } else {
        this.setData({ loading: false });
        resolve(null);
      }
    });
  },
  // loadUpRefresh() {
  //   console.log('下拉刷新');
  //   wx.showNavigationBarLoading() //在标题栏中显示加载

  //   //模拟加载
  //   setTimeout(function () {
  //     // complete
  //     wx.hideNavigationBarLoading() //完成停止加载
  //     wx.stopPullDownRefresh() //停止下拉刷新
  //   }, 1500);
  // },
  onItemClick(e) {
    let item = e.detail;
    // console.log('详情', item);
    wx.navigateTo({
      url: '../movie/movie?cid=' + item.cid,
      fail() {
        wx.showToast({
          title: '打开失败',
          icon: 'none',
          duration: 3000
        });
      }
    });
  },
  onSaveClick(e) {
    var self = this;
    let item = e.detail;
    // console.log('收藏', item);
    util.request({
      url: 'api/video/RegisterVideo',
      data: {
        id: item.cid,
        add: item.regedit ? 0 : 1
      },
      method: 'POST',
      success(rst) {
        if (rst.success) {
          var list = self.data.imgList.map(t => {
            if (t.cid == item.cid) {
              t.regedit = t.regedit ? 0 : 1;
              return t;
            }
            return t;
          });
          self.setData({
            imgList: list
          })
        }
      }
    })
  },
  onPageScroll() {

  },
  // 不知道什么鬼,都没效果.
  onPullDownRefresh() {
    this.loadUpRefresh();
  },
  onReachBottom() {
    this.loadMoreEvent();
    // this.loadMoreEvent().then(rst => {
    //   wx.stopPullDownRefresh();
    // });
  }
})
const imageUrl = 'http://image.fengyitong.name/';
function getImageList() {
  let list = [];
  for (let i = 0; i < 3; i++) {
    let item = {
      url: imageUrl + 'Blog/20170923/20170923144545',
      title: 'vlookup 查找 成绩',
      cid: parseInt(Math.random() * 1000000, 10)
    };
    list.push(item);
  }
  return list;
}

function getNavItem() {
  let list = [];
  let arr = '函数,操作,图表,数透,VBA,收藏'.split(',');
  for (let i = 0; i < arr.length; i++) {
    let obj = {
      title: arr[i],
      cid: i + 1
    }
    list.push(obj);
  }
  return list;
}