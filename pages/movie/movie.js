const util = require('../../utils/util.js')

const app = getApp()
// pages/movie/movie.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    url: '',
    cid: null,
    title: null
  },
  onLoad(option) {
    var self = this;

    util.ready(() => {
      self.setData({ cid: option.cid });
      util.request({
        url: 'api/Video/GetVideoInfo',
        data: {
          id: option.cid
        },
        success(rst) {
          // console.log(rst);
          if (rst.success) {
            self.setData({ url: rst.Data.VideoUrl, title: rst.Data.Title });
            self.playVideo();
          } else {
            util.errMessage(rst.Message);
          }
        },
      })
    });
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  playVideo() {
    this.videoContext = wx.createVideoContext('video');
    if (this.videoContext.requestFullScreen) {
      this.videoContext.requestFullScreen(90);
    }
    //debugger;
    if (this.data.title) {
      this.videoContext.sendDanmu({
        text: this.data.title,
        color: 'red'
      })
    }

    this.videoContext.play();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: this.data.title || 'Excel880实例视频教学',
      desc: 'Excel880实例视频教学',
      path: '/pages/movie/movie?cid=' + this.data.cid
    }
  }
})