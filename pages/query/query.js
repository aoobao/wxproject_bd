const util = require('../../utils/util.js')

//获取应用实例
// const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    centent_Show: true,
    searchValue: '',
    searchStr: '',
    img: '',
    nanshen_card: '',
    page: 0,
    pageCount: 1,
    imgList: [],
    scrollTop: 1,
    scrollHeight: 0,
    loading: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;

    util.ready(() => {
      self.loadMoreEvent();
    });

    wx.getSystemInfo({
      success(res) {
        self.setData({
          scrollHeight: res.windowHeight - 40
        });
      }
    });
  },

  searchHandle() {
    // console.log(this.data.searchValue);
    this.setData({ page: 0, imgList: [], pageCount: 1, searchStr: this.data.searchValue });
    this.loadMoreEvent();
  },

  searchValueInput: function (e) {
    var value = e.detail.value;
    this.setData({
      searchValue: value,
    });
  },
  onItemClick(e) {
    let item = e.detail;
    // console.log('详情', item);
    wx.navigateTo({
      url: '../movie/movie?cid=' + item.cid,
      fail() {
        wx.showToast({
          title: '打开失败',
          icon: 'none',
          duration: 3000
        });
      }
    });
  },
  onSaveClick(e) {
    var self = this;
    let item = e.detail;
    console.log('收藏', item);
    util.request({
      url: 'api/video/RegisterVideo',
      data: {
        id: item.cid,
        add: item.regedit ? 0 : 1
      },
      method: 'POST',
      success(rst) {
        if (rst.success) {
          var list = self.data.imgList.map(t => {
            if (t.cid == item.cid) {
              t.regedit = t.regedit ? 0 : 1;
              return t;
            }
            return t;
          });
          self.setData({
            imgList: list
          })
        }
      }
    })
  },
  loadMoreEvent() {
    return new Promise((resolve, reject) => {
      var self = this;
      if (this.data.page < this.data.pageCount) {
        this.setData({
          loading: true
        });
        wx.showNavigationBarLoading() //在标题栏中显示加载
        
        var ids = this.data.imgList.map(t => t.cid);

        util.request({
          url: 'api/video/GetVideoListRand',
          data: {
            title: this.data.searchStr,
            // page: this.data.page + 1,
            // pageSize: util.PAGE_SIZE
            size: util.PAGE_SIZE,
            ids: ids.toString()
          },
          success(rst) {
            // console.log(rst);
            if (rst.success) {
              const list = rst.Data.map(t => { return { url: t.ImageUrl, title: t.Code, cid: t.Id, regedit: t.Regedit }; });
              var obj = {
                imgList: [...self.data.imgList, ...list],
                loading: false,
              };
              if (list.length < util.PAGE_SIZE) obj.page = 1;
              self.setData(obj);
            }
          },
          complete() {
            self.setData({ loading: false });
            wx.hideNavigationBarLoading() //完成停止加载
            resolve();
          }
        })

      } else {
        this.setData({ loading: false });
        resolve(null);
      }
    });
  },
  suo: function (e) {
    //var id = e.currentTarget.dataset.id
    //var program_id = app.program_id;
    //var that = this;

  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})

const imageUrl = 'http://image.fengyitong.name/';
function getImageList() {
  let list = [];
  for (let i = 0; i < 3; i++) {
    let item = {
      url: imageUrl + 'Blog/20170923/20170923144545',
      title: 'vlookup 查找 成绩',
      cid: parseInt(Math.random() * 1000000, 10)
    };
    list.push(item);
  }
  return list;
}